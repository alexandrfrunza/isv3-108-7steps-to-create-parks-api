# ISV3-108 7Steps to Create - Parks api

API with parks of Moldova, information contains size and locations.

## Installation

Download full archive from [gitlab](https://gitlab.com/alexandrfrunza/isv3-108-7steps-to-create-parks-api/-/archive/main/isv3-108-7steps-to-create-parks-api-main.zip) and unzip it to empty folder.

## How to run

Use your terminal and navigate to unziped folder.

```bash
cd path/to/unziped/folder
```

run docker-compose 

```bash
docker-compose up -d
```
create db

```bash
docker-compose exec app rails db:create
```

run migrations

```bash
docker-compose exec app rails db:migrate
```

run your seeds

```bash
docker-compose exec app rails db:seed
```
## Using api


Find in unziped folder file:

```bash
parks of moldova.postman_collection.json
```

Import it in postman.
In that imported file you can find get/post/put/delete instance comands. 