# frozen_string_literal: true

class ParksController < ApplicationController
  def index
    @parks = Park.all
    render json: @parks
  end

  def show
    @park = Park.find(params[:id])
    render json: @park
  end

  def create
    @park = Park.create(params)
    render json: @park
  end

  def update
    @park = Park.find(params[:id])
    @park.update(params)
    render json: "#{@park.name} has been updated!"
  end

  def destroy
    @park = Park.find(params[:id])
    @park.destroy
    render json: "#{@park.name} has been deleted"
  end

  def params
     params.permit(:name, :size, :area, :region_id) 
  end
end
