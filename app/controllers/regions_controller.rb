# frozen_string_literal: true

class RegionsController < ApplicationController
  def index
    @regions = Region.all
    render json: @regions
  end

  def show
    @region = Region.find(params[:id])
    render json: @region
  end

  def create
    @region = Region.create(params)
    render json: @region
  end

  def update
    @region = Region.find(params[:id])
    @region.update(params)
    render json: "#{@region.name} has been updated!"
  end

  def destroy
    @region = Region.find(params[:id])
    @region.destroy
    render json: "#{@region.name} has been deleted"
  end

  def params
    parames.permit(:name, :city)
  end
end
