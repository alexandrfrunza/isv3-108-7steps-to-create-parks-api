# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

Region.destroy_all
Park.destroy_all

region1 = Region.create(name: 'Buiucani', city: 'Chisinau')
region2 = Region.create(name: 'Centru', city: 'Chisinau')
region3 = Region.create(name: 'Centru', city: 'Balti')

Park.create(name: 'La Izvor', size: 'Big', area: 150, region: region1)
Park.create(name: 'Stefan cel mare', size: 'Medium', area: 7, region: region2)
Park.create(name: 'Andries', size: 'Small', area: 3, region: region3)
