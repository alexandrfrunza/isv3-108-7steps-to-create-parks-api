# frozen_string_literal: true

class CreateParks < ActiveRecord::Migration[7.0]
  def change
    create_table :parks do |t|
      t.string :name
      t.string :size
      t.integer :area

      t.references :region, null: false, foreign_key: true

      t.timestamps
    end
  end
end
